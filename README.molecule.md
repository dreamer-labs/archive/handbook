# Provisioning Repoman for Molecule roles

To implement testing there is a certain amount of configuration that is required to get basic testing. To fully implement the testing we are going to use a number of templates that have been defined in the repoman repositories. The ones that are going to be covered in this documentation are listed below.

* commit checker – this does checks on the commit history to ensure compliance with the team guidance on commit wording.
* Lint docs – ensure that spelling is correct and documentation is formatted correctly. We use the mdlint program to ensure consistency.
* Generate release – once the merge request has been approved, the generate release script is run. This will update the change log, and create a version tag based on the type of commit that is found. 

For all of the repositories that we use, we host in gitlab, therefore we use Gitlab-CI for all of the testing. The first step is to create a gitlab-ci.yaml file to provide the configuration for the runners to run the tests.

Included in the gitlab-ci.yaml file there are a number of sections that are required. To implement the basics we need the following: 

``` yaml
stages:
  - lint
  - test
  - build
  - generate_release
  - release
```

This will define the stages that we will define later in the configuration file. Next we need to include some templates. We have these defined to help simplify using some of the repoman repositories.

``` yaml
include:
  - project: dreamer-labs/repoman/gitlab-ci-templates
    file: /.gitlab-ci-commitchecker.yml
    ref: master
  - project: dreamer-labs/repoman/gitlab-ci-templates
    file: /.gitlab-ci-lintdocs.yml
    ref: master
  - project: dreamer-labs/repoman/gitlab-ci-templates
    file: /.gitlab-ci-generate-release.yml
    ref: master
```

Once we have these defined, this will implement the basic feature of the repoman. Next we implement a scenario template of our own, this can be used to define a set of variables that we can use in our tests.

``` yaml
.scenario_template: &scenario_template
  image: registry.gitlab.com/dreamer-labs/repoman/dl-molecule-stable/image:latest
  variables:
    DEVEL_SLUG: "devel"
    DOCKER_TLS_CERTDIR: ""
    DOCKER_HOST: "tcp://docker:2375"
  services:
    - docker:dind 
  except:
    variables:
      - $CI_COMMIT_MESSAGE =~ /^chore\(release\)/
  only:
    - merge_requests
    - master
```

The image in he above snipped has been built to to use the same version of molecule and ansible that we are using for deployments.

Finally we can define our tests. 

``` yaml
molecule_test:
  <<: *scenario_template
  stage: test
  script:
    - molecule test
```

---

## Configuration files

Now that we have the configuration file in place. We need to finish setting up the repository. This included copy over a number of files that are needed to configure the different scripts that will be run as part of the templates.

For the commit linter, we need a configuration file added to the root of the repository.

* commitlint.config.js
  * This configuration file defines the rules used for the commit lint check script, but it can be modified as needed.

``` javascript
module.exports = {
        rules: {
                'body-leading-blank': [1, 'always'],
                'footer-leading-blank': [1, 'always'],
                'header-max-length': [2, 'always', 72],
                'scope-case': [2, 'always', 'lower-case'],
                'subject-case': [
                        0,
                        'never',
                        ['sentence-case', 'start-case', 'pascal-case', 'upper-case']
                ],
                'subject-empty': [2, 'never'],
                'subject-full-stop': [2, 'never', '.'],
                'type-case': [2, 'always', 'lower-case'],
                'type-empty': [2, 'never'],
                'type-enum': [
                        2,
                        'always',
                        [
                                'build',
                                'chore',
                                'ci',
                                'docs',
                                'feat',
                                'fix',
                                'perf',
                                'refactor',
                                'revert',
                                'style',
                                'test'
                        ]
                ]
        }
};
```

The lint-docs container need a few different configuarion files added to the root of the repository.

* .mdlrc
  * This file containers the configuration for the markdown linter.
    https://github.com/markdownlint/markdownlint

``` yaml
rules "~MD013"
```

* .pyspelling.yml
  * Provides the configuartion for the spell checker

``` yaml
---

matrix:
  - name: Check Build
    hunspell:
      d: en_US,dictionary/en_US.dic
    dictionary:
      wordlists:
        - ./allowed_words.dic
    sources:
      - ./README.md
      - ./dev/README.md
    pipeline:
      - pyspelling.filters.context:
          context_visible_first: true
          escapes: '\\[\\`~]'
          delimiters:
            - open: '(?s)^(?P<open>^::\n\n*)'
              content: '.*?'
              close: '(?=(^[^\s]))'
            - open: '(?s)^(?P<open> *`{3,})$'
              close: '^(?P=open)$'
            # Ignore text between inline back ticks
            - open: '(?P<open>`+)'
              content: '.*?'
              close: '(?P=open)'
            - open: ' '
              content: '[a-zA-Z0-9\-]{15,}'
              close: ''
            # Ignore oneline sphinx directives
            - open: '(?P<open>\.\. .+\:+)'
              content: '.*?'
              close: '$'
            - open: '(?P<open>\.\. .+\n\n*)'
              content: '.*?'
              close: '$'

...

```

* allowed_words.dic
  * List of words that are not in the standard dictionary that should be ignored.

``` yaml
Ansible
ManIAC
PWD
ansible
backend
config
declutters
deps
dev
distro
entrypoint
entrypoints
env
localhost
pre
pullling
repo
repos
subdir
subdirectories
subdirectory
testfile
trainwreck
v0
ve
yaml
yml
CentOS
RHEl
SELinux
audit2allow
idempotency
selinux
Galera
galera
ip
mysql
mysqld
ssl
wsrep
OOM
afcyber
oom
tcp
te
udp
mirrorlist
url
```

---

The Generate Release needs a configuration file and an update to the repo configuration inside of gitlab.

* .releaserc.json
  * Contains the configuration for the symantic release scripts to increment versioning and create the release

``` json
{
    "plugins": [
        "@semantic-release/commit-analyzer",
        "@semantic-release/release-notes-generator",
                ["@semantic-release/changelog", {
           "changelogFile": "CHANGELOG.md"
        }],
        ["@semantic-release/exec", {
            "prepareCmd": "echo ${nextRelease.version} > VERSION"
        }],
        ["@semantic-release/git", {
             "assets": ["VERSION", "CHANGELOG.md"],
             "message": "chore(release): Bumped to Version ${nextRelease.version}\n\n${nextRelease.notes}"
        }]
    ]
}

```
## Deploy Key
The last change that is needed, we need to add a deployment key to the CI/CD configuration, and allow write access the repo. To update this you will need at least maintainer accees to the repo, so you may need to get with a maintiner/owner to configure this.
